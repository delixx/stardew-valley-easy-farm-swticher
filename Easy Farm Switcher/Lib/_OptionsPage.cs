﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StardewModdingAPI;
using StardewValley;
using StardewValley.Menus;
using HarmonyLib;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Custom_Farm_Loader;
using Custom_Farm_Loader.Menus;
using StardewValley.Buildings;
using StardewValley.Objects;
using Microsoft.Xna.Framework.Input;

namespace Easy_Farm_Switcher.Lib
{
    public class _OptionsPage
    {
        public static Mod Mod;
        public static IMonitor Monitor;
        public static IModHelper Helper;

        private static Texture2D CustomFarmIcon;
        private static ClickableTextureComponent CustomFarmButton;
        private static string HoverText = "";

        private static IClickableMenu ParentMenu = null;
        private static CustomFarmSelection FarmSelectionMenu = null;
        private static DialogueBox ConfirmationMenu = null;
        public static void Initialize()
        {
            Mod = ModEntry.Mod;
            Monitor = ModEntry._Monitor;
            Helper = ModEntry._Helper;

            CustomFarmIcon = Helper.ModContent.Load<Texture2D>("assets/CustomFarmIcon.png");

            var harmony = new Harmony(Mod.ModManifest.UniqueID);

            harmony.Patch(
                original: AccessTools.Method(typeof(OptionsPage), nameof(OptionsPage.draw), new[] { typeof(SpriteBatch) }),
                postfix: new HarmonyMethod(typeof(_OptionsPage), nameof(_OptionsPage.draw_Postfix))
            );

            harmony.Patch(
                original: AccessTools.Method(typeof(OptionsPage), nameof(OptionsPage.performHoverAction)),
                postfix: new HarmonyMethod(typeof(_OptionsPage), nameof(_OptionsPage.performHoverAction_Postfix))
            );

            harmony.Patch(
                original: AccessTools.Method(typeof(OptionsPage), nameof(OptionsPage.postWindowSizeChange)),
                postfix: new HarmonyMethod(typeof(_OptionsPage), nameof(_OptionsPage.postWindowSizeChange_Postfix))
            );

            harmony.Patch(
                original: AccessTools.Constructor(typeof(OptionsPage), new[] { typeof(int), typeof(int), typeof(int), typeof(int) }),
                postfix: new HarmonyMethod(typeof(_OptionsPage), nameof(_OptionsPage.constructor_Postfix))
            );

            harmony.Patch(
                original: AccessTools.Method(typeof(OptionsPage), nameof(OptionsPage.receiveLeftClick)),
                prefix: new HarmonyMethod(typeof(_OptionsPage), nameof(_OptionsPage.receiveLeftClick_Prefix))
            );

            harmony.Patch(
                original: AccessTools.Method(typeof(OptionsPage), nameof(OptionsPage.applyMovementKey), new[] { typeof(int) }),
                prefix: new HarmonyMethod(typeof(_OptionsPage), nameof(_OptionsPage.applyMovementKey_Prefix))
            );
        }

        public static void draw_Postfix(OptionsPage __instance, SpriteBatch b)
        {
            if (!Context.IsMainPlayer)
                return;

            CustomFarmButton.draw(b);
            if (HoverText != "")
                IClickableMenu.drawHoverText(b, HoverText, Game1.smallFont);

            return;
        }

        public static void performHoverAction_Postfix(OptionsPage __instance, int x, int y)
        {
            HoverText = "";

            if (!Context.IsMainPlayer)
                return;

            if (!GameMenu.forcePreventClose)
                CustomFarmButton.tryHover(x, y);

            if (CustomFarmButton.containsPoint(x, y))
                HoverText = CustomFarmButton.hoverText;
        }

        public static void postWindowSizeChange_Postfix(OptionsPage __instance)
        {
            if (!Context.IsMainPlayer)
                return;

            createClickableTextures(__instance);
        }

        public static void constructor_Postfix(OptionsPage __instance, int x, int y, int width, int height)
        {
            if (!Context.IsMainPlayer)
                return;

            createClickableTextures(__instance);
        }

        public static void createClickableTextures(OptionsPage __instance)
        {
            CustomFarmButton = new ClickableTextureComponent("CustomFarm",
                    new Rectangle(__instance.xPositionOnScreen - 48,
                    __instance.yPositionOnScreen + 100, 54, 60),
                    null, Helper.Translation.Get("change_farm"),
                    CustomFarmIcon,
                    new Rectangle(0, 0, 18, 20), 3f) {
                myID = 548,
                upNeighborID = -99998,
                leftNeighborID = -99998,
                rightNeighborID = -99998,
                downNeighborID = -99998
            };
        }

        public static bool receiveLeftClick_Prefix(OptionsPage __instance, int x, int y, bool playSound = true)
        {
            if (GameMenu.forcePreventClose || !Context.IsMainPlayer)
                return true;

            if (CustomFarmButton.containsPoint(x, y)) {
                Game1.playSound("drumkit6");

                FarmSelectionMenu = new CustomFarmSelection(true, false);
                ParentMenu = Game1.activeClickableMenu;
                (Game1.activeClickableMenu = FarmSelectionMenu).exitFunction = exitFarmSelectionMenu;

                return false;
            }

            return true;
        }

        public static bool applyMovementKey_Prefix(OptionsPage __instance, int direction)
        {
            if (!Context.IsMainPlayer || direction != 3 || __instance.currentlySnappedComponent is null || __instance.currentlySnappedComponent.myID >= __instance.options.Count)
                return true;

            var currentOption = __instance.options[__instance.currentlySnappedComponent.myID + __instance.currentItemIndex];

            if (currentOption is OptionsDropDown or OptionsPlusMinus or OptionsPlusMinusButton or OptionsInputListener or OptionsSlider)
                return true;

            __instance.currentlySnappedComponent = CustomFarmButton;
            __instance.snapCursorToCurrentSnappedComponent();
            return false;
        }

        public static void exitFarmSelectionMenu()
        {
            var customFarm = FarmSelectionMenu.CurrentCustomFarm;
            if (Game1.GetFarmTypeID() == customFarm.ID) {
                Game1.activeClickableMenu = ParentMenu;
                FarmSelectionMenu = null;
                ParentMenu = null;
                return;
            }

            var yesNo = Game1.currentLocation.createYesNoResponses();
            ConfirmationMenu = new DialogueBox(Helper.Translation.Get("farm_confirmation", new { name = customFarm.Name }), yesNo);
            Game1.currentLocation.afterQuestion = exitConfirmation;
            Game1.activeClickableMenu = ConfirmationMenu;
        }

        public static void exitConfirmation(Farmer who, string whichAnswer)
        {
            var customFarm = FarmSelectionMenu.CurrentCustomFarm;

            ConfirmationMenu = null;
            FarmSelectionMenu = null;

            if (whichAnswer == "No") {
                Game1.activeClickableMenu = ParentMenu;
                ParentMenu = null;
                return;
            }

            ParentMenu = null;
            CustomFarmSelection.setWhichFarm(customFarm);
            Game1.activeClickableMenu = ParentMenu;
            EntityTransfer.reloadMapFeatures();
        }

    }
}
