﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Custom_Farm_Loader;
using Custom_Farm_Loader.Menus;
using StardewValley.Buildings;
using StardewValley.Objects;
using StardewValley;
using StardewValley.TerrainFeatures;
using StardewModdingAPI;
using StardewValley.Menus;

namespace Easy_Farm_Switcher.Lib
{

    public class EntityTransfer
    {
        public static Mod Mod;
        public static IMonitor Monitor;
        public static IModHelper Helper;

        private static List<Building> LeftOverBuildings = null;
        private static List<StardewValley.Object> LeftOverObjects = null;
        private static List<Item> LeftOverItems = null;
        private static bool[] PassedWorldEdge = new bool[] { false, false, false, false };

        public static void Initialize()
        {
            Mod = ModEntry.Mod;
            Monitor = ModEntry._Monitor;
            Helper = ModEntry._Helper;
        }

        public static void reloadMapFeatures()
        {
            var f = Game1.getFarm();
            moveBuildingsOutOfTheWay(f);
            f.mapPath.Set("Maps\\" + Farm.getMapNameFromTypeInt(Game1.whichFarm));

            resetCachedPositions(f);
            f.reloadMap();
            Game1.startingCabins = f.buildings.Where(e => e.isCabin).Count();

            f.largeTerrainFeatures.Clear();
            f.resourceClumps.Clear();
            f.debris.Clear();

            saveObjects(f);
            saveFurniture(f);
            saveTerrainFeatures(f);

            f.MakeMapModifications();
            moveInitialBuildings(f);

            ItemGrabMenu.organizeItemsInList(LeftOverItems);
            LeftOverItems.RemoveAll(el => el == null);

            f.loadObjects();
            f.objects.Clear();
            f.terrainFeatures.Clear();
            f.resourceClumps.Clear();

            placeSavedObjects(f);
            warpAnimalsHome(f);
            moveLeftoverBuildings(f);
        }

        private static void clearArea(GameLocation location, int startingX, int startingY, int width, int height)
        {
            for (int x = startingX; x < startingX + width; x++)
                for (int y = startingY; y < startingY + height; y++)
                    location.removeEverythingFromThisTile(x, y);
        }

        private static void moveBuildingsOutOfTheWay(Farm f)
        {
            //If I don't do this there'll be weird spots in the water
            foreach (var b in f.buildings) {
                b.tileX.Set(1000);
                b.tileY.Set(1000);
            }
        }
        public static void cleanBuildingAreas(Farm f)
        {
            foreach (var b in f.buildings)
                clearArea(f, b.tileX.Value, b.tileY.Value, b.tilesWide.Value, b.tilesHigh.Value);
        }

        public static void resetCachedPositions(Farm f)
        {
            f.mapMainMailboxPosition = null;
            f.mapGrandpaShrinePosition = null;
            f.mapShippingBinPosition = null;
            f.mapSpouseAreaCorner = null;
            f.mainFarmhouseEntry = null;
        }

        private static List<Vector2> getStartingCabinLocations(Farm f)
        {
            var ret = new List<Vector2>();

            var paths_layer = f.map.GetLayer("Paths");
            if (paths_layer == null)
                return ret;

            for (int x = 0; x < paths_layer.LayerWidth; x++)
                for (int y = 0; y < paths_layer.LayerHeight; y++)
                    if (paths_layer.Tiles[x, y] != null && paths_layer.Tiles[x, y].TileIndex == 30)
                        ret.Add(new Vector2(x, y));

            return ret;
        }

        public static void moveInitialBuildings(Farm f)
        {
            var startingCabinLocations = getStartingCabinLocations(f);

            var cabin = 0;
            bool initialShippingBinPlaced = false;
            bool initialPetBowlPlaced = false;
            bool initialGreenhousePlaced = false;
            LeftOverBuildings = new List<Building>();

            foreach (var b in f.buildings) {
                if (b.isCabin && cabin < startingCabinLocations.Count()) {
                    b.tileX.Set((int)startingCabinLocations[cabin].X);
                    b.tileY.Set((int)startingCabinLocations[cabin].Y);
                    cabin++;
                    continue;
                }

                if (b is ShippingBin && !initialShippingBinPlaced) {
                    initialShippingBinPlaced = true;
                    var s = f.GetStarterShippingBinLocation();
                    b.tileX.Set((int)s.X);
                    b.tileY.Set((int)s.Y);
                    continue;
                }

                if (b is GreenhouseBuilding && !initialGreenhousePlaced) {
                    initialGreenhousePlaced = true;
                    var s = f.GetGreenhouseStartLocation();
                    b.tileX.Set((int)s.X);
                    b.tileY.Set((int)s.Y);
                    continue;
                }

                if (b is PetBowl && !initialPetBowlPlaced) {
                    initialPetBowlPlaced = true;
                    var s = getLegacyPetBowl(f);

                    if (s.X == -1 && s.Y == -1)
                        s = f.GetStarterPetBowlLocation();
                    b.tileX.Set((int)s.X);
                    b.tileY.Set((int)s.Y);
                    continue;
                }

                if (b.buildingType.Value == "Farmhouse") {
                    var s = getActualStarterFarmhouseLocation(f);
                    b.tileX.Set((int)s.X);
                    b.tileY.Set((int)s.Y);
                    continue;
                }

                b.tileX.Set(1000);
                b.tileY.Set(1000);
                LeftOverBuildings.Add(b);
            }
        }

        public static void finalize(Farm f)
        {
            LeftOverObjects.Clear();
            LeftOverItems.Clear();
            LeftOverBuildings.Clear();

            f.updateWarps();
            f.largeTerrainFeatures.Clear();
            f.loadObjects();
            cleanBuildingAreas(f);
            updateInteriorWarps(f);

            var entry = f.GetMainFarmHouseEntry();
            Game1.warpFarmer("Farm", entry.X, entry.Y + 1, 2);
        }

        public static void updateInteriorWarps(Farm f)
        {
            foreach (var b in f.buildings)
                b.updateInteriorWarps();
        }

        public static void warpAnimalsHome(Farm f)
        {
            for (int l = f.animals.Count() - 1; l >= 0; l--)
                f.animals.Pairs.ElementAt(l).Value.warpHome();
        }

        public static void moveLeftoverBuildings(Farm f)
        {
            if (LeftOverBuildings.Count == 0) {
                finalize(f);
                return;
            }

            (Game1.activeClickableMenu = new MoveBuildingMenu(LeftOverBuildings)).exitFunction = delegate {
                finalize(f);
            };
        }

        public static void placeSavedObjects(Farm f)
        {
            var angle = 0;
            var level = 0;
            var subLevel = 0;
            PassedWorldEdge = new bool[] { false, false, false, false };

            foreach (var obj in LeftOverObjects) {
                var pos = getSpiralPosition(f, ref level, ref subLevel, ref angle);

                obj.TileLocation = pos.ToVector2();
                f.setObject(pos.ToVector2(), obj);
            };

            //Place leftover items in new chests
            if (LeftOverItems.Count == 0)
                return;

            var chest = new Chest(true);
            var pos2 = getSpiralPosition(f, ref level, ref subLevel, ref angle);
            f.setObject(pos2.ToVector2(), chest);
            foreach (var item in LeftOverItems)
                if (chest.addItem(item) != null) {
                    chest = new Chest(true);
                    pos2 = getSpiralPosition(f, ref level, ref subLevel, ref angle);
                    f.setObject(pos2.ToVector2(), chest);
                    chest.addItem(item);
                }
        }

        private static Point getSpiralPosition(Farm f, ref int level, ref int subLevel, ref int angle)
        {
            var entry = f.GetMainFarmHouseEntry();
            Point pos = new Point(0, 0);

            if (angle == 0) { //Left Side of Farmhouse
                pos = new Point(entry.X - 6 - level, entry.Y - 5 - level + subLevel);

                subLevel++;
                if (subLevel == 7 + 2 * level) {
                    subLevel = 0;
                    angle++;
                }
            } else if (angle == 1) { //Bottom Side of Farmhouse
                pos = new Point(entry.X - 6 - level + subLevel, entry.Y + 2 + level);

                if (pos.X == entry.X) { //Making sure the player can still exit the farmhouse
                    subLevel++;
                    pos = new Point(entry.X - 6 - level + subLevel, entry.Y + 2 + level);
                }

                subLevel++;
                if (subLevel == 11 + 2 * level) {
                    subLevel = 0;
                    angle++;
                }
            } else if (angle == 2) { //Right Side of Farmhouse
                pos = new Point(entry.X + 4 + level, entry.Y + 1 + level - subLevel);

                if (pos.X == entry.X + 4 && pos.Y == entry.Y + 1) { //Making sure the chest doesn't get placed on the new mailbox
                    subLevel++;
                    pos = new Point(entry.X + 4 + level, entry.Y + 1 + level - subLevel);
                }

                subLevel++;
                if (subLevel == 7 + 2 * level) {
                    subLevel = 0;
                    angle++;
                }
            } else { //Top Side of Farmhouse
                pos = new Point(entry.X + 4 + level - subLevel, entry.Y - 6 + level);

                subLevel++;
                if (subLevel == 11 + 2 * level) {
                    level++;
                    subLevel = 0;
                    angle = 0;
                }
            }

            int mapTileWidth = f.Map.DisplayWidth / 64;
            int mapTileHeight = f.Map.DisplayHeight / 64;

            if (pos.X < 0)
                PassedWorldEdge[0] = true;
            if (pos.Y < 0)
                PassedWorldEdge[1] = true;
            if (pos.X >= mapTileWidth)
                PassedWorldEdge[2] = true;
            if (pos.Y >= mapTileHeight)
                PassedWorldEdge[3] = true;

            if (PassedWorldEdge.All(e => e == true))
                throw new Exception("Critical Map Conversion Error: The new farm is too small to contain all the objects.\n" +
                    "Please restart the game and try again after removing some objects");

            if (!f.CanItemBePlacedHere(pos.ToVector2()))
                return getSpiralPosition(f, ref level, ref subLevel, ref angle);

            return pos;
        }

        public static void addItemFromFruitTree(FruitTree t)
        {
            var obj = ItemRegistry.Create<StardewValley.Object>(t.treeId.Value);
            LeftOverItems.Add(obj);
        }

        public static void addItemFromHoeDirt(HoeDirt h)
        {
            if (h.crop is null || h.crop.netSeedIndex.Value is null)
                return;

            LeftOverItems.Add(ItemRegistry.Create<StardewValley.Object>(h.crop.netSeedIndex.Value));
        }

        public static void addItemFromFlooring(Flooring flooring)
        {
            if (flooring.GetData()?.ItemId is string itemId)
                LeftOverItems.Add(ItemRegistry.Create(itemId));
        }

        public static void saveTerrainFeatures(Farm f)
        {
            foreach (var tf in f.terrainFeatures.Pairs)
                if (tf.Value is Flooring)
                    addItemFromFlooring(tf.Value as Flooring);

                else if (tf.Value is HoeDirt)
                    addItemFromHoeDirt(tf.Value as HoeDirt);

                else if (tf.Value is FruitTree)
                    addItemFromFruitTree(tf.Value as FruitTree);

                else if (tf.Value is Bush bush && bush.size.Value == 3) { //size 3 = greenTeaBush

                    if (bush.modData.Keys.Contains("furyx639.CustomBush/Id")) //Support for Custom Bush framework
                        LeftOverItems.Add(ItemRegistry.Create(bush.modData["furyx639.CustomBush/Id"]));
                    else
                        LeftOverItems.Add(ItemRegistry.Create("(O)251"));
                }

            f.terrainFeatures.Clear();
        }

        public static void saveFurniture(Farm f)
        {
            foreach (var furniture in f.furniture)
                LeftOverItems.Add(furniture);

            f.furniture.Clear();
        }

        public static void saveObjects(Farm f)
        {
            LeftOverItems = new List<Item>();
            LeftOverObjects = new List<StardewValley.Object>();

            foreach (var obj in f.Objects.Pairs) {

                    //Chests
                    if (obj.Value is Chest chest
                        && (chest.SpecialChestType == Chest.SpecialChestTypes.None || chest.SpecialChestType == Chest.SpecialChestTypes.BigChest)
                        && !chest.isEmpty()) {

                        if (chest.mutex.IsLockHeld())
                            chest.mutex.ReleaseLock();
                        LeftOverObjects.Add(obj.Value);
                        continue;
                    }

                    if (ShouldIgnoreObject(obj.Value))
                        continue;

                    else if (isSprinkler(obj.Value))
                        continue;

                    else if (isIdleBigCraftable(obj.Value))
                        continue;

                    else if (obj.Value.bigCraftable.Value
                            && obj.Value.isPlaceable()
                            && !obj.Value.IsScarecrow())

                        LeftOverObjects.Add(obj.Value);

                    else if (obj.Value.isPlaceable())
                        LeftOverItems.Add(obj.Value);
                }

            f.objects.Clear();
        }

        public static bool isSprinkler(StardewValley.Object obj)
        {
            if (!obj.IsSprinkler())
                return false;

            if (obj.heldObject.Value != null) {
                if (obj.heldObject.Value.heldObject.Value is Chest chest)
                    LeftOverItems.AddRange(chest.Items);


                LeftOverItems.Add(obj.heldObject.Value);
            }


            LeftOverItems.Add(obj);
            return true;
        }

        public static bool isIdleBigCraftable(StardewValley.Object obj)
        {
            var knownExceptions = new string[] {
            "Tapper",
            "Bee House",
            };

            //Turning idle big craftables and their heldobject into items
            if (!obj.bigCraftable.Value)
                return false;

            if (obj.isPlaceable() && obj.MinutesUntilReady == 0) {
                if (obj.heldObject.Value != null)
                    LeftOverItems.Add(obj.heldObject.Value);

                obj.heldObject.Value = null;
                LeftOverItems.Add(obj);
                return true;

            } else if (knownExceptions.Contains(obj.Name)) {
                LeftOverItems.Add(obj);
                return true;
            }

            return false;
        }

        public static bool ShouldIgnoreObject(StardewValley.Object obj)
        {
            return obj.Name == "Unlockable Shop"
                || obj.Name == "UnlockableBundles Shop"
                || obj.IsWeeds()
                || obj.name.Equals("Stone")
                || obj.name.Equals("Twig")
                || obj is Fence;
        }

        //f.getStarterFarmhouseLocation will return me the Farmhouse building tilelocation if it exists, which makes it useless for me
        //also kinda different from what the name says it does, works different from other 
        public static Vector2 getActualStarterFarmhouseLocation(Farm f)
        {
            if (!f.TryGetMapPropertyAs("FarmHouseEntry", out Point parsed, required: false)) {
                parsed = new Point(64, 15);
            }

            return new Vector2(parsed.X - 5, parsed.Y - 3);
        }


        //In 1.6 the pet bowl is a building, but most farms will still have the building layer pet bowl tile
        public static Vector2 getLegacyPetBowl(Farm f)
        {
            var building_layer = f.map.GetLayer("Buildings");
            for (int x = 0; x < building_layer.LayerWidth; x++)
                for (int y = 0; y < building_layer.LayerHeight; y++)
                    if (building_layer.Tiles[x, y] != null && building_layer.Tiles[x, y].TileIndex == 1938) {
                        return new Vector2(x - 1, y);
                    }

            return new Vector2(-1, -1);
        }

    }
}