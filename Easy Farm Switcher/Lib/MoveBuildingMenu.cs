﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StardewValley;
using StardewValley.Objects;
using StardewValley.Buildings;
using StardewValley.Menus;
using StardewModdingAPI;
using xTile.Dimensions;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using StardewValley.BellsAndWhistles;
using StardewValley.TokenizableStrings;

namespace Easy_Farm_Switcher.Lib
{
    public class MoveBuildingMenu : CarpenterMenu
    {
        public static Mod Mod;
        public static IMonitor Monitor;
        public static IModHelper Helper;
        public static void Initialize()
        {
            Mod = ModEntry.Mod;
            Monitor = ModEntry._Monitor;
            Helper = ModEntry._Helper;
        }

        //These fields are there for mod compaitibility
        public bool magicalConstruction = false;
        //private List<BluePrint> blueprints = new List<BluePrint>();

        private List<Building> Buildings;
        private int BuildingsIndex = 0;
        public MoveBuildingMenu(List<Building> buildings) : base("Robin", null)
        {
            Buildings = buildings;
            if (!Buildings.Any())
                exitThisMenu(false);

            setUpForBuildingPlacement();
            cancelButton.bounds.X = 10000;
            onFarm = true;
            Action = CarpentryAction.Move;
            buildingToMove = Buildings[BuildingsIndex];
            buildingToMove.isMoving = true;
        }

        public override void receiveLeftClick(int x, int y, bool playSound = true)
        {
            //base.receiveLeftClick(x, y, playSound);
            baseLeftClickMoving(x, y);

            if (buildingToMove == null) {
                BuildingsIndex++;

                if (Buildings.Count == BuildingsIndex) {
                    exitThisMenu(false);

                    Game1.player.viewingLocation.Value = null;
                    Game1.displayHUD = true;
                    Game1.viewportFreeze = false;
                    Game1.viewport.Location = new Location(320, 1536);
                    Game1.displayFarmer = true;

                } else {
                    onFarm = true;
                    buildingToMove = Buildings[BuildingsIndex];
                }

            }
        }

        public void baseLeftClickMoving(int x, int y)
        {
            if (((Farm)Game1.getLocationFromName("Farm")).buildStructure(buildingToMove, new Vector2((Game1.viewport.X + Game1.getMouseX(ui_scale: false)) / 64, (Game1.viewport.Y + Game1.getMouseY(ui_scale: false)) / 64), Game1.player)) {
                buildingToMove.isMoving = false;
                if (buildingToMove is ShippingBin) {
                    (buildingToMove as ShippingBin).initLid();
                }
                if (buildingToMove is GreenhouseBuilding) {
                    Game1.getFarm().greenhouseMoved.Value = true;
                }
                buildingToMove.performActionOnBuildingPlacement();
                buildingToMove = null;
                Game1.playSound("axchop");
                DelayedAction.playSoundAfterDelay("dirtyHit", 50);
                DelayedAction.playSoundAfterDelay("dirtyHit", 150);
            } else {
                Game1.playSound("cancel");
            }
        }

        public override void draw(SpriteBatch b)
        {
            base.draw(b);

            var displayName = TokenParser.ParseText(buildingToMove.GetData()?.Name);
            if (displayName is not null)
                SpriteText.drawStringWithScrollBackground(b, displayName, Game1.uiViewport.Width / 2 - SpriteText.getWidthOfString(displayName) / 2, 96);
        }

        //Prevents people from closing the moving menu with escape
        public override bool readyToClose() => false;
    }
}
