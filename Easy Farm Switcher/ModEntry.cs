﻿using System;
using StardewValley;
using StardewValley.Menus;
using StardewModdingAPI;
using System.Collections.Generic;

namespace Easy_Farm_Switcher
{

    public class ModEntry : Mod
    {

        public static Mod Mod;
        public static IMonitor _Monitor;
        public static IModHelper _Helper;

        public override void Entry(IModHelper helper)
        {
            Mod = this;
            _Monitor = Monitor;
            _Helper = Helper;

            Lib._OptionsPage.Initialize();
            Lib.EntityTransfer.Initialize();
            Lib.MoveBuildingMenu.Initialize();
        }
    }
}
